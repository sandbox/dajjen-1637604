<?php
/**
 * @file
 * Template file for outputting common HTML-elements.
 */
?>

<!-- Headings -->
<h1>Heading 1</h1>
<h2>Heading 2</h2>
<h3>Heading 3</h3>
<h4>Heading 4</h4>
<h5>Heading 5</h5>
<h6>Heading 6</h6>

<!-- Tabs -->
<div class="tabs">
		<h2 class="element-invisible">Primary tabs</h2>
		<ul class="tabs primary">
			<li class="active"><a class="active" href="#">View<span class="element-invisible">(active tab)</span></a></li>

			<li><a href="#">Edit</a></li>
			<li><a href="#">Devel</a></li>
	</ul>
	</div>

<!-- Buttons -->
<div><input name="button" type="button" value="Button" /></div>
<div><input name="button" type="button" disabled="disabled" value="Button" /></div>

<!-- Radiobutton -->
<div style="margin:1em 0">
  <label>Label for Radios</label>
  <input name="radio" type="radio" value="" />
</div>

<!-- Checkbox -->
<div style="margin:1em 0">
  <label>Label for Checkboxes</label>
  <input name="checkboxes" type="checkbox" value="" />
</div>

<!-- Textfield -->
 <div style="margin:1em 0">
  <label for="textfield">Label</label>
  <input id="textfield" type="text" value="input" size="50" maxlength="12"/>
</div>

<!-- Textarea -->
<div style="margin:1em 0">
  <label>Label for Textarea</label>
  <textarea id="text" rows="5" cols="50"></textarea>
</div>

<!-- Filefield -->
<div style="margin:1em 0">
  <label>label for Filefield</label>
  <input name="files" type="file" size="50" />
</div>

<!-- Selectlist -->
<div style="margin:1em 0">
  <select>
  	<option>Option 1</option>
  	<option>Option 2</option>
  	<option>Option 3</option>
  	<option>Option 4</option>
  </select>
</div>

<!-- Form -->
<form id="form" method="post" accept-charset="UTF-8" action="/action" onsubmit="alert(0)">
	<fieldset>
		<!-- Form buttons -->
		<div><input name="button" type="button" value="Button" /></div>
		<div><input name="button" type="button" disabled="disabled" value="Button" /></div>

		<!-- Form Textfield -->
	 	<div style="margin:1em 0">
	  	<label for="textfield">Label</label>
	  	<input id="textfield" type="text" value="input" size="50" maxlength="12"/>
		</div>

		<!--Form Textarea -->
		<div style="margin:1em 0">
		  <label>Label for Textarea</label>
		  <textarea id="text" rows="5" cols="50"></textarea>
		</div>
	</fieldset>
</form>


<p>0 1 2 3 4 5 6 7 8 9</p>
<p>+ - √≥  _  = ? ! ~ ( ) [ ] { } * ^ | "" '' `¬• / \ &amp; @ ¬£ $ √ü Œ© % ¬Æ ; : . ,</p>

<p>a b c d e f g h i j k l m n o p q r s t u v w x y z ÀÜ ‚Ä∞ √Ç</p>
<p>A B C D E F G H I J K L M N O P Q R S T U V W X Y Z √∑ ∆í ‚âà</p>

<h2>H2 Heading with Paragraphs</h2>
<p>Paragraph one. Neo damnum aliquam consequat. Abdo genitus luptatum virtus singularis distineo validus ea. Gravis oppeto huic incassum torqueo jumentum dolus esca suscipit. Vulputate hos singularis veniam zelus singularis utinam vulputate sino quidne. Nutus neque quidem. Natu interdico melior.Oppeto vulpes meus jugis ut iriure valde consectetuer abigo. Aptent bene ideo commoveo quadrum abbas. Vereor macto neo probo ille.</p>

<h3>H3 Subheading</h3>
<p>Paragraph two. Esse vulpes sino virtus rusticus brevitas mos. Cui ad vulputate et vero zelus feugiat os olim obruo. Facilisi quadrum proprius gravis velit humo nunc wisi imputo antehabeo.Valde dolore tation facilisi ullamcorper gemino gemino cui qui paratus. Camur in capto euismod. Brevitas vero iriure letalis valde oppeto plaga ullamcorper adipiscing inhibeo.</p>

<p><a href="#">Hyperlink</a></p>
<p><span>Span</span></p>
<p><abbr title="abbr">abbr</abbr></p>
<p><acronym title="acronym">acronym</acronym></p>
<address>address</address>
<p><bdo dir="rtl">LTR bdo</bdo></p>
<p><bdo dir="ltr">LTR bdo</bdo></p>
<blockquote><p>blockquote</p></blockquote>
<p><q>q</q></p>
<p><cite>cite</cite></p>
<pre>Pre</pre>
<p><kbd>kbd</kbd></p>
<p><samp>samp</samp></p>
<p><var>var</var></p>
<del>del</del>
<ins>ins</ins>
<p><dfn>dfn</dfn></p>
<p><strong>strong</strong></p>
<p><em>em</em></p>
<p>sup<sup>1</sup></p>
<p>sub<sub>1</sub></p>

<table>
	<caption>Caption. Table with border 1, width 50%</caption>
	<thead>
	  <tr>
		  <th>thead th</th>
		  <th>thead th</th>
		  <th>thead th</th>
	  </tr>
	</thead>
	<tbody>
		<tr>
		  <td>tbody tr td</td>
		  <td>tbody tr td</td>
		  <td>tbody tr td</td>
		</tr>
		<tr>
		  <td>Neo damnum aliquam consequat.</td>
		  <td>Neo damnum aliquam consequat.</td>
		  <td>Neo damnum aliquam consequat. </td>
		</tr>
		<tr>
		  <td>Neo damnum aliquam consequat.</td>
		  <td>Neo damnum aliquam consequat. Neo damnum aliquam consequat.</td>
		  <td>Neo damnum aliquam consequat. Neo damnum aliquam consequat. Neo damnum aliquam consequat</td>
		</tr>
	</tbody>
</table>

<ol>
	<li>Ordered list item</li>
	<li>Ordered list item</li>
	<li>Ordered list item</li>
	<li>Ordered list item</li>
</ol>

<ul>
	<li>Unordered list item</li>
	<li>Unordered list item</li>
	<li>Unordered list item</li>
	<li>Unordered list item</li>
</ul>

<dl>
	<dt>document term</dt>
	<dd>document definition</dd>
	<dt>document term</dt>
	<dd>document definition</dd>
</dl>
